const { Router } = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const HomeController = require('../controllers/web/HomeController')

const web = Router()

web.use(bodyParser.json())
web.use(bodyParser.urlencoded({ extended: true }))
web.use(cookieParser())

const homeController = new HomeController
web.get('/', homeController.index)

module.exports = web
