const bodyParser = require('body-parser')
const { Router } = require('express')
const ResponseMiddleware = require('../middlewares/ResponseMiddleware');

const UserController = require('../controllers/api/UserController.js')

const api = Router()
const userController = new UserController()

api.use(bodyParser.json())
api.use(ResponseMiddleware())

api.get('/user', userController.getUser)

module.exports = api
